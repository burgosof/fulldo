@component('mail::message')
	@include('emails.partials.mailHeader')
	<h1 class="main-title">Actualización sobre Solicitud de cambio de jefatura</h1>
	<p class="content">
		Estimado(a), se ingresado una actualización sobre una solicitud de cambio de jefatura.

		Detalles: {{$move_request->cargo->funcionario->name}} {{$move_request->cargo->funcionario->apellido}} pasara de la jefatura 
<b>{{$move_request->requester->name}} {{$move_request->requester->apellido}}</b> a 
<b>{{$move_request->target->funcionario->name}} {{$move_request->target->funcionario->apellido}}</b>
		<br>Para conocer el estatus detallado visite su pagina de administración de subordinados
	</p>
	<table style="width: 100%;text-align:center;margin-top:50px;" cellspacing="0" cellpadding="0">
      	<tr>
          	<td style="text-align:center;">
              	<table style="margin:0 auto;" cellspacing="0" cellpadding="0">
                 	 <tr>
                      	<td style="border-radius: 2px;text-align:center;" bgcolor="#337ab7">
                          	<a href="{{route('workers')}}" target="_blank" style="padding: 8px 12px; border: 1px solid #337ab7;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;">
                                  Ir a Administrar
                              </a>
                      	</td>
                  	</tr>
              	</table>
          	</td>
      	</tr>
    </table>
@endcomponent
