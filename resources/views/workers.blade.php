@extends("layouts.general")
@section("page_styles")
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-typeahead/2.10.6/jquery.typeahead.js"> 
   <style>
        .emp-profile {
            padding: 3%;
            margin-top: 3%;
            margin-bottom: 3%;
            border-radius: 0.5rem;
            background: #fff;
        }

        .profile-img {
            text-align: center;
        }

        .profile-img img {
            max-height: 370px;
            max-width: 370px;
            width: 100%;
            height: 100%;
        }

        .profile-img .file {
            position: relative;
            overflow: hidden;
            margin-top: -20%;
            width: 70%;
            border: none;
            border-radius: 0;
            font-size: 15px;
            background: #212529b8;
        }

        .table .avatar{
            width: 1px;
        }

        .profile-img .file input {
            position: absolute;
            opacity: 0;
            right: 0;
            top: 0;
        }

        .profile-head h5 {
            color: #333;
        }

        .profile-head h6 {
            color: #1D0D89;
        }

        .profile-edit-btn {
            border: none;
            border-radius: 1.5rem;
            width: 70%;
            padding: 2%;
            font-weight: 600;
            color: #6c757d;
            cursor: pointer;
        }

        .proile-rating {
            font-size: 12px;
            color: #818182;
        }

        .proile-rating span {
            color: #495057;
            font-size: 15px;
            font-weight: 600;
        }

        .profile-head .nav-tabs {
            margin-bottom: 5%;
        }

        .profile-head i {
            margin-right: 5px;
            width: 20px;
            text-align: center;
        }


        .profile-head .nav-tabs .nav-link {
            font-weight: 600;
            border: none;
        }

        .profile-head .nav-tabs .nav-link.active {
            border: none;
            border-bottom: 2px solid #1D0D89;
        }

        .profile-work {
            padding: 14%;
            margin-top: -15%;
        }

        .profile-work p {
            font-size: 12px;
            color: #818182;
            font-weight: 600;
            margin-top: 10%;
        }

        .profile-work a {
            text-decoration: none;
            color: #495057;
            font-weight: 600;
            font-size: 14px;
        }

        .profile-work ul {
            list-style: none;
        }

        .profile-tab label {
            font-weight: 600;
        }

        .profile-tab p {
            font-weight: 600;
            color: #1D0D89;
        }
        .fontsize10{
            font-size: 10px;
        }
        .fontsize12{
            font-size: 12px;
        }
        .fontsize13{
            font-size: 13px;
        }
        .table-stats .table {
            cursor: pointer;
        }
        .m-t-20{
          margin-top: 20px;
        }
        .td-email{
            text-transform:none !important;
        }
	.btn-circle.btn-xl {
    width: 70px;
    height: 70px;
    padding: 10px 16px;
    border-radius: 35px;
    font-size: 24px;
    line-height: 1.33;
}

.btn-circle {
    width: 30px;
    height: 30px;
    padding: 6px 0px;
    border-radius: 15px;
    text-align: center;
    font-size: 12px;
    line-height: 1.42857;
}

.dTypeahead {
  position: relative;
  *z-index: 1;
  width: 500px;
  margin: 50px auto 0 auto;
  padding: 15px;
  text-align: left;
  background-color: #0097cf;
  background-image: -moz-linear-gradient(top, #04a2dd, #03739c);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#04a2dd), to(#03739c));
  background-image: -webkit-linear-gradient(top, #04a2dd, #03739c);
  background-image: -o-linear-gradient(top, #04a2dd, #03739c);
  background-image: linear-gradient(top, #04a2dd, #03739c);
  background-repeat: repeat-x;
  border: 1px solid #024e6a;
  -webkit-border-radius: 10px;
     -moz-border-radius: 10px;
          border-radius: 10px;
  -webkit-box-shadow: 0 0 2px #111;
     -moz-box-shadow: 0 0 2px #111;
          box-shadow: 0 0 2px #111;
}

.Typeahead-spinner {
  position: absolute;
  top: 7px;
  right: 7px;
  display: none !important;
  width: 28px;
  height: 28px;
}

.Typeahead-hint,
.Typeahead-hint2,
.Typeahead-input {
  width: 100%;
  padding: 5px 8px;
  font-size: 24px;
  line-height: 30px;
  border: 1px solid #024e6a;
  -webkit-border-radius: 8px;
     -moz-border-radius: 8px;
          border-radius: 8px;
}

.Typeahead-hint, .Typeahead-hint2 {
  position: absolute;
  top: 0;
  left: 0;
  color: #ccd6dd;
  opacity: 1;
}

.Typeahead-input {
  position: relative;
  background-color: transparent;
  background-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
  outline: none;
}

.Typeahead-menu, .Typeahead-menu2 {
  position: absolute;
  top: 95%;
  left: 2.5%;
  z-index: 100;
  display: none;
  width: 95%;
  margin-bottom: 20px;
  overflow: hidden;
  background-color: #fff;
  -webkit-border-radius: 8px;
     -moz-border-radius: 8px;
          border-radius: 8px;
          box-shadow: 0px 0px 0px 1px green;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
     -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
          box-shadow: 0 5px 10px rgba(0,0,0,.2);
}

.Typeahead-menu.is-open, .Typeahead-menu2.is-open {
  display: block;
}

.Typeahead-selectable {
  cursor: pointer;
}

.Typeahead-selectable + .Typeahead-selectable {
  border-top: 1px solid #ccd6dd;
}

/* ProfileCard */

.ProfileCard {
  position: relative;
  padding: 8px;
}

.ProfileCard-avatar {
  position: absolute;
  top: 8px;
  left: 8px;
  width: 52px;
  height: 52px;
  border: 2px solid #ccd6dd;
  border-radius: 5px;
}

.ProfileCard:hover .ProfileCard-avatar {
  border-color: #f5f8fa;
}

.ProfileCard-details {
  min-height: 60px;
  padding-left: 60px;
}

.ProfileCard-realName,
.ProfileCard-screenName {
  display: inline-block;
}

.ProfileCard-realName {
  font-weight: 700;
}

.ProfileCard-screenName {
  color: #8899a6;
}

.ProfileCard-description {
  margin-top: 5px;
  font-size: 14px;
  line-height: 18px;
}

.ProfileCard-stats {
  float: right;
  text-align: right;
}

.ProfileCard-stat {
  display: inline-block;
  font-size: 12px;
  line-height: 16px;
  text-transform: uppercase;
}

.ProfileCard-stat-label {
  color: #8899a6;
  font-weight: 500;
}

.ProfileCard-stat + .ProfileCard-stat {
  margin-left: 5px;
}

.ProfileCard:hover,
.ProfileCard.is-active {
  color: #fff;
  background: #55acee;
}

.ProfileCard:hover .ProfileCard-screenName,
.ProfileCard:hover .ProfileCard-stat-label,
.ProfileCard.is-active .ProfileCard-screenName,
.ProfileCard.is-active .ProfileCard-stat-label {
  color: #fff;
}

/* EmptyMessage */

.EmptyMessage {
  position: relative;
  padding: 10px;
  font-size: 24px;
  line-height: 30px;
  text-align: center;

}
    </style>
@endsection
<!-- Content -->
@section("content")
    <div class="container emp-profile">
        <form method="post">
            <div class="row">
                <div class="col-md-12 card" style="padding:30px;">
			<button type="button" class="btn btn-success  reverseModaler" data-toggle="modal" data-target="#exampleModal2"><i class="fa fa-plus"></i> Añadir Subordinado
                            </button>

                        <div class="table-stats ov-h">
@if($admin)
<div class="card-body">
                            <h5 class="box-title">Mis aprobaciones (como administrador)</h5>
                        </div>

<!--Table-->
<table id="tablePreview" class="table">
<!--Table head-->
  <thead>
    <tr>
      <th>ID</th>
      <th>Trabajador</th>
      <th>Jefatura Actual</th>
      <th>Nueva Jefatura</th>
      <th>Aprobación Jefatura</th>
      <th>Aprobacion Administrador</th>
      <th>Estado</th>
        <th>Acciones</th>
    </tr>
  </thead>
  <!--Table head-->
  <!--Table body-->
  <tbody>
@foreach($aprobaciones_admin as $solicitud)
    <tr>
      <th scope="row">{{$solicitud->id}}</th>
      <td>{{ $solicitud->cargo->funcionario->name . ' ' . $solicitud->cargo->funcionario->apellido }}</td>
      <td>{{ $solicitud->requester->name . ' ' . $solicitud->requester->apellido }}</td>
      <td>{{ $solicitud->target->id == auth()->user()->id ? "Usted": $solicitud->target->funcionario->name . ' ' . $solicitud->target->funcionario->apellido }}</td>
      <td>{{ $solicitud->new_jefatura_aprove == 0 ? "NO" : "SI" }}</td>
      <td>{{ $solicitud->admin_aprove == 0 ? "NO" : "SI"  }}</td>
<td>
	@if($solicitud->status == 0)
	Pendiente
	@elseif($solicitud->status == 1)
	Completada
	@else
	Rechazada
	@endif
      </td>
	

<td>
@if($solicitud->status == 0 && !$faked)
<button type="button" class="btn btn-danger btn-circle refuseRequestAdmin" data-request-id="{{ $solicitud->id }}"><i class="fa fa-times"></i>
                            </button>
<button type="button" class="btn btn-success btn-circle aproveRequestAdmin" data-request-id="{{ $solicitud->id }}"><i class="fa fa-check"></i>
                            </button>
@endif
</td>
    </tr>
@endforeach

  </tbody>
  <!--Table body-->
</table>
<!--Table-->

@endif
<div class="card-body">
                            <h5 class="box-title">Mis aprobaciones (como jefatura)</h5>
                        </div>

<!--Table-->
<table id="tablePreview" class="table">
<!--Table head-->
  <thead>
    <tr>
      <th>ID</th>
      <th>Trabajador</th>
      <th>Jefatura Actual</th>
      <th>Nueva Jefatura</th>
      <th>Aprobación Jefatura</th>
      <th>Aprobacion Administrador</th>
      <th>Estado</th>
	<th>Acciones</th>
    </tr>
  </thead>
  <!--Table head-->
  <!--Table body-->
  <tbody>


















@foreach($aprobaciones as $solicitud)
    <tr>
      <th scope="row">{{$solicitud->id}}</th>
      <td>{{ $solicitud->cargo->funcionario->name . ' ' . $solicitud->cargo->funcionario->apellido }}</td>
      <td>{{ $solicitud->requester->name . ' ' . $solicitud->requester->apellido }}</td>
      <td>{{ $solicitud->target->id == auth()->user()->id ? "Usted": $solicitud->target->funcionario->name . ' ' . $solicitud->target->funcionario->apellido }}</td>
      <td>{{ $solicitud->new_jefatura_aprove == 0 ? "NO" : "SI" }}</td>
      <td>{{ $solicitud->admin_aprove == 0 ? "NO" : "SI"  }}</td>
<td>
        @if($solicitud->status == 0)
        Pendiente
        @elseif($solicitud->status == 1)
        Completada
        @else
        Rechazada
        @endif
      </td>
<td>
@if($solicitud->status == 0 && !$faked)
<button type="button" class="btn btn-danger btn-circle refuseRequest" data-request-id="{{ $solicitud->id }}"><i class="fa fa-times"></i>
                            </button>
<button type="button" class="btn btn-success btn-circle aproveRequest" data-request-id="{{ $solicitud->id }}"><i class="fa fa-check"></i>
                            </button>
@endif
    </tr>
@endforeach



@foreach($aprobaciones_reverse as $solicitud)
    <tr>
      <th scope="row">{{$solicitud->id}}</th>
      <td>{{ $solicitud->cargo->funcionario->name . ' ' . $solicitud->cargo->funcionario->apellido }}</td>
      <td>{{ $solicitud->requester->name . ' ' . $solicitud->requester->apellido }}</td>
      <td>{{ $solicitud->target->id == auth()->user()->id ? "Usted": $solicitud->target->funcionario->name . ' ' . $solicitud->target->funcionario->apellido }}</td>
      <td>{{ $solicitud->new_jefatura_aprove == 0 ? "NO" : "SI" }}</td>
      <td>{{ $solicitud->admin_aprove == 0 ? "NO" : "SI"  }}</td>
<td>
        @if($solicitud->status == 0)
        Pendiente
        @elseif($solicitud->status == 1)
        Completada
        @else
        Rechazada
        @endif
      </td>
<td>
@if($solicitud->status == 0 && !$faked)
<button type="button" class="btn btn-danger btn-circle refuseRequest" data-request-id="{{ $solicitud->id }}"><i class="fa fa-times"></i>
                            </button>
<button type="button" class="btn btn-success btn-circle aproveRequest" data-request-id="{{ $solicitud->id }}"><i class="fa fa-check"></i>
                            </button>
@endif
    </tr>
@endforeach



  </tbody>
  <!--Table body-->
</table>
<!--Table-->

<div class="card-body">
                            <h5 class="box-title">Solicitudes realizadas</h5>
                        </div>

<!--Table-->
<table id="tablePreview" class="table">
<!--Table head-->
  <thead>
    <tr>
      <th>ID</th>
      <th>Trabajador</th>
      <th>Jefatura Actual</th>
      <th>Nueva Jefatura</th>
      <th>Aprobacion Jefatura Nueva</th>
      <th>Aprobacion Administrador</th>
      <th>Estado</th>
    </tr>
  </thead>
  <!--Table head-->
  <!--Table body-->
  <tbody>
@foreach($solicitudes as $solicitud)
    <tr>
      <th scope="row">{{$solicitud->id}}</th>
      <td>{{ $solicitud->cargo->funcionario->name . ' ' . $solicitud->cargo->funcionario->apellido }}</td>
      <td>{{ $solicitud->requester->name . ' ' . $solicitud->requester->apellido }}</td>
      <td>{{ $solicitud->target->funcionario->name . ' ' . $solicitud->target->funcionario->apellido }}</td>
      <td>{{ $solicitud->new_jefatura_aprove == 0 ? "NO" : "SI" }}</td>
      <td>{{ $solicitud->admin_aprove == 0 ? "NO" : "SI"  }}</td>

<td>
        @if($solicitud->status == 0)
        Pendiente
        @elseif($solicitud->status == 1)
        Completada
        @else
        Rechazada
        @endif
      </td>


    </tr>
@endforeach

  </tbody>
  <!--Table body-->
</table>
<!--Table-->
                        </div>
                    @if($cargo && count($cargo->subCargos)>0)
                        <div class="card-body">
                            <h5 class="box-title">Reportes Directos</h5>
                        </div>
                        <div class="table-stats ov-h">
                            <table class="table" id="reports-contacts">
                                <tbody>
                                    @foreach ($cargo->subCargos as $subCargo)
                                        @if ($subCargo->funcionario != null)
                                            <tr class="pb-0" data-url="{{ route('perfil') }}?id={{$subCargo->funcionario->id}}">
                                                <td class="avatar">
                                                    <div class="round-img">
                                                        <img class="rounded-circle" src="images/avatar/{{$subCargo->funcionario->foto}}" alt="">
                                                    </div>
                                                </td>
                                                <td><span class="fontsize13">{{$subCargo->nombre}}</span>
                                                    <br>
                                                    <span class="fontsize12">{{ $subCargo->funcionario->name . ' ' . $subCargo->funcionario->apellido }}</span>
</td>
						
                                                <td>
                           <!-- <button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i>
                            </button>-->
				@if($subCargo->moveProcesses()->where("status", "=",0)->count() == 0)
                            <button type="button" class="btn btn-danger btn-circle userModaler" data-cargo-id="{{$subCargo->id}}" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-times"></i>
                            </button>
				@else
				<span class="badge badge-info">En Proceso</span>
				@endif
				</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </form>
    </div>
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
         <nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Añadir como Subordinado</a>
  </div>
</nav>

      </div>
      <div class="modal-body">
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <span>Busque y seleccione el colaborador a mover.</span>
        <div class="Typeahead Typeahead--twitterUsers">
              <div class="u-posRelative">
                <input class="Typeahead-input demo-input" id="demo-input" type="text" name="q" placeholder="Nombre o RUT">
                <img class="Typeahead-spinner" src="img/spinner.gif">
              </div>
              <div class="Typeahead-menu"></div>
            </div>

</div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">...</div>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" id="submit_btn_reverse" class="btn btn-primary" disabled>Solicitar Cambio</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel_btn_reverse">Cancelar</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
	 <nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Jefatura Incorrecta</a>
    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Cargo Incorrecto</a>
  </div>
</nav>

      </div>
      <div class="modal-body">
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
	<span>Busque y seleccione la jefatura actual del colaborador.</span>
	<div class="Typeahead Typeahead--twitterUsers">
              <div class="u-posRelative">
                <input class="Typeahead-input demo-input" id="demo-input2" type="text" name="q" placeholder="Nombre o RUT">
                <img class="Typeahead-spinner" src="img/spinner.gif">
              </div>
              <div class="Typeahead-menu2"></div>
            </div>

</div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">...</div>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" id="submit_btn" class="btn btn-primary" disabled>Solicitar Cambio</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel_btn">Cancelar</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section("page_scripts")
<script src="https://twitter.github.io/typeahead.js/js/handlebars.js"></script>
    <script src="https://twitter.github.io/typeahead.js/js/jquery-1.10.2.min.js"></script>
    <script src="https://twitter.github.io/typeahead.js/js/jquery.xdomainrequest.min.js"></script>
    <script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script> 
<script>
	$(document).ready(function() {
var userActive = null;
var moveType = 0;
$(".userModaler").on("click", function(e){
	moveType = 0;
	userActive = $(this).data("cargoId");
	console.log(userActive, "boom")
})
$(".reverseModaler").on("click", function(e){
        moveType = 1;
})




  var engine, remoteHost, template, empty;

  $.support.cors = true;

  remoteHost = '/toolbox';
  template = Handlebars.compile($("#result-template").html());
  empty = Handlebars.compile($("#empty-template").html());

  engine = new Bloodhound({
    identify: function(o) { return o.id; },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name', 'apellido'),
    dupDetector: function(a, b) { return a.id === b.id; },
    remote: {
      url: remoteHost + '?q=%QUERY%',
      wildcard: '%QUERY%'
    }
  });

  // ensure default users are read on initialization
 // engine.get('1090217586', '58502284', '10273252', '24477185')

  function engineWithDefaults(q, sync, async) {
    if (q === '') {
      //sync(engine.get('1090217586', '58502284', '10273252', '24477185'));
      async([]);
    }

    else {
      engine.search(q, sync, async);
    }
  }
var selected_jefatura = false;































  $('#demo-input').typeahead({
    hint: $('.Typeahead-hint'),
    menu: $('.Typeahead-menu'),
    minLength: 0,
    classNames: {
      open: 'is-open',
      empty: 'is-empty',
      cursor: 'is-active',
      suggestion: 'Typeahead-suggestion',
      selectable: 'Typeahead-selectable'
    }
  }, {
    source: engineWithDefaults,
    displayKey: 'name',
	display: function(item){ return item.name+' '+item.apellido},
    templates: {
      suggestion: template,
      empty: empty
    }
  })
  .on('typeahead:select', function(e, item) {

	if(moveType == 0){
		selected_jefatura = item;
		console.log("nueva jef selecionada", e, item)
		$("#submit_btn").removeAttr("disabled")
	} else if(moveType == 1){
		selected_jefatura = item;
                console.log("seleccionado miembro a mover", e, item)
                $("#submit_btn_reverse").removeAttr("disabled")
	}



  }).on('typeahead:change', function(e,item) {

	if((selected_jefatura.name+' '+selected_jefatura.apellido) != item){
		selected_jefatura = false;
		if(moveType == 0){
			$("#submit_btn").attr("disabled", "disabled")
		} else if(moveType == 1){
			$("#submit_btn_reverse").attr("disabled", "disabled")
		}
	}
	console.log("change",e, item)	
})
$('#demo-input2').typeahead({
    hint: $('.Typeahead-hint2'),
    menu: $('.Typeahead-menu2'),
    minLength: 0,
    classNames: {
      open: 'is-open',
      empty: 'is-empty',
      cursor: 'is-active',
      suggestion: 'Typeahead-suggestion',
      selectable: 'Typeahead-selectable'
    }
  }, {
    source: engineWithDefaults,
    displayKey: 'name',
        display: function(item){ return item.name+' '+item.apellido},
    templates: {
      suggestion: template,
      empty: empty
    }
  })
  .on('typeahead:select', function(e, item) {

        if(moveType == 0){
                selected_jefatura = item;
                console.log("nueva jef selecionada", e, item)
                $("#submit_btn").removeAttr("disabled")
        } else if(moveType == 1){
                selected_jefatura = item;
                console.log("seleccionado miembro a mover", e, item)
                $("#submit_btn_reverse").removeAttr("disabled")
        }



  }).on('typeahead:change', function(e,item) {

        if((selected_jefatura.name+' '+selected_jefatura.apellido) != item){
                selected_jefatura = false;
                if(moveType == 0){
                        $("#submit_btn").attr("disabled", "disabled")
                } else if(moveType == 1){
                        $("#submit_btn_reverse").attr("disabled", "disabled")
                }
        }
        console.log("change",e, item)
})

$("#submit_btn").on("click", function() {
    $("#submit_btn").attr("disabled", "disabled")
    $.post("/request-relocation", {
        cargo_id: userActive,
        jefatura_id: selected_jefatura.id,
        _token: $("meta[name='csrf-token']").attr("content")
    }, function(response) {
        console.log("response", response);
        if (response.status) {
            Swal.fire(
                'Solicitud creada',
                'Se ha enviado la solicitud de aprobación a la nueva jefatura y al administrador.',
                'success'
            ).then((result) => {
                window.location.reload()
            })
        } else {
            Swal.fire(
                'Solicitud NO creada',
                'Prueba mas tarde si el problema persiste contacta a un administrador.',
                'error'
            ).then((result) => {
                window.location.reload()
            })
        }
        $('#cancel_btn').trigger('click');
    })

})
$("#submit_btn_reverse").on("click", function() {
    $("#submit_btn_reverse").attr("disabled", "disabled")
    $.post("/request-relocation-reverse", {
        cargo_id: selected_jefatura.cargo.id,
        jefatura_id: null,
        _token: $("meta[name='csrf-token']").attr("content")
    }, function(response) {
        console.log("response", response);
        if (response.status) {
            Swal.fire(
                'Solicitud creada',
                'Se ha enviado la solicitud de aprobación a la antigua jefatura y al administrador.',
                'success'
            ).then((result) => {
                window.location.reload()
            })
        } else {
            Swal.fire(
                'Solicitud NO creada',
                'Prueba mas tarde si el problema persiste contacta a un administrador.',
                'error'
            ).then((result) => {
                window.location.reload()
            })
        }
        $('#cancel_btn_reverse').trigger('click');
    })

})

$(".refuseRequest").on("click", function(e){
    $.post("/update-relocation", {
                change_type: 0,
                change: 1,
                request_id: $(this).data("requestId"),
                _token: $("meta[name='csrf-token']").attr("content")
        }, function(response){
                console.log("response", response);
                if(response.status){
                    Swal.fire(
  'Solicitud rechazada',
  'Se ha rechazado la solicitud.',
  'error'
).then((result) => {
        window.location.reload()
})

                }
        })

})
$(".aproveRequest").on("click", function(e){

    $.post("/update-relocation", {
                change_type: 0,
                change: 2,
                request_id: $(this).data("requestId"),
                _token: $("meta[name='csrf-token']").attr("content")
        }, function(response){
                console.log("response", response);
                if(response.status){
                    Swal.fire(
  'Solicitud aprobada',
  'Se ha aprobado la solicitud.',
  'success'
).then((result) => {
        window.location.reload()
})

                }
        })
})
$(".refuseRequestAdmin").on("click", function(e){
$.post("/update-relocation", {
                change_type: 1,
                change: 1,
                request_id: $(this).data("requestId"),
                _token: $("meta[name='csrf-token']").attr("content")
        }, function(response){
                console.log("response", response);
                if(response.status){
                    Swal.fire(
  'Solicitud rechazada',
  'Se ha rechazado la solicitud.',
  'error'
).then((result) => {
        window.location.reload()
})

                }
        })
})
$(".aproveRequestAdmin").on("click", function(e){
    $.post("/update-relocation", {
                change_type: 1,
                change: 2,
                request_id: $(this).data("requestId"),
                _token: $("meta[name='csrf-token']").attr("content")
        }, function(response){
                console.log("response", response);
                if(response.status){
                    Swal.fire(
  'Solicitud aprobada',
  'Se ha aprobado la solicitud.',
  'success'
).then((result) => {
        window.location.reload()
})

                }
        })
})



});
</script>
 <script id="result-template" type="text/x-handlebars-template">
      <div class="ProfileCard u-cf">
        <img class="ProfileCard-avatar" src="images/avatar/@{{ foto }}">

        <div class="ProfileCard-details">
          <div class="ProfileCard-realName">@{{name}} @{{apellido}}</div>
          <div class="ProfileCard-description"> @{{cargo.nombre}}</div>
        </div>

      </div>
    </script>

    <script id="empty-template" type="text/x-handlebars-template">
      <div class="EmptyMessage">0 resultados</div>
    </script>

<script type="text/javascript">

    $(function() {
    });

</script>
@endsection
