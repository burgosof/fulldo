<nav class="navbar navbar-expand-sm navbar-default">
    <div id="main-menu" class="main-menu collapse navbar-collapse">
        <ul class="nav navbar-nav">
            @php
                $user=auth()->user();
		$user = App\User::where("id", "=", $user->id)->with("cargo.subCargos")->first();
            @endphp
            @foreach(menu() as $item)
                @if($user->perfil <= $item->permissions && $item->order > 0)
                    @if($item->subItems->count())
                        <li class="menu-title">{{$item->title}}</li>
                        @foreach($item->subItems as $subItem)
                            @if($user->perfil <= $subItem->permissions)
                                <li class="menu-item-has-children dropdown">
                                    <a href="{{$subItem->target()}}" class="dropdown-toggle" data-toggle="dropdown"
                                       aria-haspopup="true"
                                       aria-expanded="false"> <i class="menu-icon {{$subItem->icon}}"></i>{{$subItem->title}}
                                    </a>
                                    @if($subItem->subItems->count())
                                        <ul class="sub-menu children dropdown-menu">
                                            @foreach($subItem->subItems as $sub)
                                                @if($user->perfil <= $sub->permissions)
                                                    <li>
                                                        <i class="{{$sub->icon }}"></i>
                                                        <a href="{{$sub->target()}}">{{$sub->title}}</a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endif
                        @endforeach
                    @else
                        <li class="">
                            <a href="{{$item->target()}}"><i class="menu-icon {{$item->icon }}"></i>{{$item->title}}</a>
                        </li>
                    @endif
                @endif
            @endforeach
	@if($user->perfil == 0 || $user->perfil == 2 || ( $user->cargo && $user->cargo->subCargos && count($user->cargo->subCargos) >= 1) || ( $user->cargo && $user->cargo->subCargos && count($user->cargo->movementsAsJefatura) >= 1))
                <li class="">
                            <a href="/subordinados"><i class="menu-icon fa fa-user-plus"></i>Subordinados</a>
                        </li>
        @endif

        </ul>
    </div>
</nav>
