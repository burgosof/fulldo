@extends("default.show")

@section("form")
	<a class="btn btn-info" style="margin: 15px 15px 20px 0 !important" href="http://do.fulldo.cl/export?holding_id={{ $instance->id}}">Exportar Excel</a><br>
    @include("partials.field",["name"=>"nombre","title"=>"Nombre"])
    @include("partials.field",["name"=>"descripcion","title"=>"Descripcion"])
    @include("partials.field",["type"=>"color","name"=>"color","title"=>"Color"])
    @include("partials.image",["name"=>"logo","title"=>"Logo","folder"=>"holdings"])
    @include("partials.switch",["name"=>"estado","title"=>"Estado","value"=>$instance->estado])
@endsection
