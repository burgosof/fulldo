<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\{User, UserMovementProcess};

class NewChangeRequestEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($moverequest)
    {
        $this->move_request = $moverequest;
    }
    //

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->move_request = UserMovementProcess::where('id',$this->move_request->id)->with("cargo.funcionario", "target.funcionario", "requester")->first();
        return $this->subject('Nueva Solicitud de Cambio de Jefatura')
                   ->from('example@fulldo.com')
                   ->markdown('emails.newmoverequest')
                   ->with([
                    'move_request' => $this->move_request
        ]);
    }
}
