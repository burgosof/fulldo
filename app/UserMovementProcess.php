<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMovementProcess extends Model
{
/*
$new_request->cargo_id = $request->cargo_id;
                $user = User::where('id',$request->jefatura_id)->first();
                $cargo = $user->cargo;
                $new_request->target_jefatura = $cargo->id;
*/
	public function scopeRegularMove($query){
		return $query->where("type", "=", 0);
	}
	public function scopeReverseMove($query){
                return $query->where("type", "=", 1);
        }
	public function scopeResignMove($query){
                return $query->where("type", "=", 2);
        }


    public function cargo()
    {
        return $this->belongsTo(Cargo::class, "cargo_id", "id");
    }
    public function requester()
    {
        return $this->belongsTo(User::class, "requester_id", "id");
    }

	public function target()
    {
        return $this->belongsTo(Cargo::class, "target_jefatura", "id");
    }
}
