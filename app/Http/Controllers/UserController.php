<?php

namespace App\Http\Controllers;

use App\Cargo;
use App\UserMovementProcess;
use App\Exceptions\SelfDeleteException;
use App\Jobs\NotifyUserOfCompletedImport;
use App\Mail\EmailUserLogin;
use App\Mail\NewPasswordEmail;

use App\Mail\NewChangeRequestEmail;
use App\Mail\UpdateChangeRequestEmail;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Log;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class UserController extends Controller
{
    protected $clazz = User::class;
    protected $resource = "users";
    protected $rules = [
        "email" => "required|email|unique:users,email,{id},id",
        "rut" => 'required|max:12|unique:users,rut,{id},id',
        'holding_id' => 'required|exists:ma_holding,id',
        'empresa_id' => 'required|exists:ma_empresa,id',
        'gerencia_id' => 'required|exists:ma_gerencia,id',
        'cargo_id' => 'nullable|exists:ma_cargo,id',
        'foto_file' => 'file|image| max:1000',
    ];
    protected $importer= \App\Imports\UsuariosImport::class;

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws ValidationException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */

    public function edit($id, Request $request)
    {
        $instance = User::find($id);
        if ($instance) {
            if ($request->ajax())
                return response()->json($instance, 200);
            return view("$this->resource.edit", compact("instance"));
        }
        throw new ResourceNotFoundException("$this->clazz with id " . $request->route()->parameter("id"));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'password' => 'confirmed',
        ]);
        $passw = $request->get("password");
        $request->merge(["password" => Hash::make($request->get("password", "123456"))]);
        $result = parent::store($request);
        $cargo = Cargo::find($request->get("cargo_id"));
        if ($cargo) {
            $cargo->id_funcionario = $request->new_id;
            $cargo->update();
        }

        try{
            Mail::to($request->get("email"))->send(new EmailUserLogin($request->get("email"),$passw) );
        }
        catch(\Exception $e){
            \Log::info('Error Sending Mail: '.$e->getMessage());
        }

        return $result;
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'password' => 'nullable|confirmed',
        ]);
        if ($request->get("password")){
            $new_pass=$request->get("password");
            $request->merge(["password" => Hash::make($new_pass)]);
            try{
                Mail::to($request->get("email"))
                ->send(new NewPasswordEmail($new_pass,$id));
            }
            catch(\Exception $e){ 
                \Log::info('-------Error Sending Mail: '.$e->getMessage());
            }
        }
        $result = parent::update($id, $request);

        $cargo_anterior = User::find($id)->cargo;
        //REMOVE from last cargo
        if ($cargo_anterior && $cargo_anterior->id != $request->get("cargo_id")) {
            if ($cargo_anterior) {
                $cargo_anterior->id_funcionario = null;
                $cargo_anterior->update();
            }
        }
        //Add to new Cargo
        $cargo = Cargo::find($request->get("cargo_id"));
        if ($cargo && $cargo->id_funcionario != $id) {
            $cargo->id_funcionario = $id;
            $cargo->update();
        }
        return $result;
    }

    public function changepassword(Request $request)
    {
        $data = $request->only(['password', 'repassword']);
        $user = Auth::user();
        $user->password = Hash::make($data['password']);
        $user->password_changed_at = date('Y-m-d H:i:s');
        return response()->json(['success' => $user->save()], 200);
    }

    public function destroy($id, Request $request)
    {
        if ($id == auth()->user()->id) {
            throw new SelfDeleteException();
        }
        return parent::destroy($id, $request);
    }

    public function profile(Request $request){
        setlocale(LC_ALL, 'es_ES'); 

        $data = $request->only('id');
        if(count($data))
            $user = User::where('id',$data['id'])->first();
        else
            $user = auth()->user();
        $cargo = $user->cargo;
        if(isset($cargo))
            $jefatura = $cargo->jefatura;
        else
            $jefatura = null;
        
        return view('profile', [ 'user' => $user,
                                'cargo' => $cargo,
                                'jefatura' => $jefatura,
                            ]);
    }


	public function workers(Request $request){
        setlocale(LC_ALL, 'es_ES');
	$faked = false;
        $data = $request->only('id');
            $user = auth()->user();
	$user = User::where('id',$user->id)->first();
	if($user->perfil == 0 && count($data) ){

$user = User::where('id', "=",$data['id'])->first();
$faked = true;
}
        $cargo = $user->cargo;
	if(!$cargo){
		$cargo = new \stdClass;
		$cargo->id = 999999999;
		$cargo->faked = true;
		$cargo->subCargos = [];

	}
//	RegularMove()
	$solicitudes = UserMovementProcess::where("requester_id", "=", $user->id)->orWhere(function($query) use($cargo) {$query->where("type","=", 1)->where("target_jefatura", "=", $cargo->id); })->with("cargo.funcionario", "target.funcionario", "requester")->get();
	
	$aprobaciones = UserMovementProcess::RegularMove()->where("target_jefatura", "=", $cargo ? $cargo->id : "novalidasjefatura")->with("cargo.funcionario", "target.funcionario", "requester")->get();
	$aprobaciones_reverse = UserMovementProcess::ReverseMove()->where("requester_id", "=", $user->id )->with("cargo.funcionario", "target.funcionario", "requester")->get();
        if(isset($cargo) && !isset($cargo->faked))
            $jefatura = $cargo->jefatura;
        else
            $jefatura = null;
	
$aprobaciones_admin = UserMovementProcess::whereHas("target.funcionario",function($q)use($user){ 

	if($user->perfil ==2) {
		$q->where("empresa_id", "=", $user->empresa_id); 
	} 
})->with("cargo.funcionario", "target.funcionario", "requester")->get();	
        return view('workers', [ 'user' => $user,
                                'cargo' => $cargo,
                                'jefatura' => $jefatura,
				'solicitudes' => $solicitudes,
				'aprobaciones' => $aprobaciones,
                                'aprobaciones_reverse' => $aprobaciones_reverse,
				'aprobaciones_admin' => $aprobaciones_admin,
				'admin' => $user->perfil == 2 || $user->perfil == 0,
				'faked' => $faked
                            ]);
    }

   public function changeJefatura(Request $request){
		$new_request = new UserMovementProcess();
		$new_request->requester_id = auth()->user()->id;
		$new_request->cargo_id = $request->cargo_id;
		$user = User::where('id',$request->jefatura_id)->first();
        	$cargo = $user->cargo;
		$new_request->target_jefatura = $cargo->id;
		$new_request->new_jefatura_aprove = 0;
 		$new_request->admin_aprove = 0;
		$new_request->status = 0;
		$new_request->type = 0;
		if($request->change_cargo_desc == 1){
			/*$old_cargo = Cargo::find($request->cargo_id);
			$old_cargo->nombre = $request->new_cargo_desc;
			$old_cargo->save();*/
			$new_request->new_cargo_desc = $request->new_cargo_desc;			
		}
		$new_request->save();
 try{
		$admin_empresa = User::where("perfil", '=', 2)->where("empresa_id", '=',$user->empresa_id)->pluck("email");
              \Mail::to($user->email)->cc($admin_empresa)->bcc("j.roure@outlook.cl")->send(new NewChangeRequestEmail($new_request));
            }
            catch(\Exception $e){
                \Log::info('-------Error Sending Mail to jefatura: '.$e->getMessage());
            }

   return response()->json(["status" => !is_null($new_request)]);
}
public function changeJefaturaReverse(Request $request){
                $new_request = new UserMovementProcess();
		
		$old_jefatura = Cargo::find($request->cargo_id);
		$old_jefatura = Cargo::find($old_jefatura->id_jefatura);
                $new_request->requester_id = $old_jefatura->id_funcionario;
		$old_user_jefatura = User::find($old_jefatura->id_funcionario);

                $new_request->cargo_id = $request->cargo_id;


                $user = User::where('id', auth()->user()->id)->first();
                $cargo = $user->cargo;

                $new_request->target_jefatura = $cargo->id;
                $new_request->new_jefatura_aprove = 0;
                $new_request->admin_aprove = 0;
                $new_request->status = 0;
                $new_request->type = 1;
                $new_request->save();
/*
 try{
                $admin_empresa = User::where("perfil", '=', 2)->where("empresa_id", '=',$user->empresa_id)->pluck("email");
              
		\Mail::to($old_user_jefatura->email)->cc($admin_empresa)->bcc("j.roure@outlook.cl")->send(new NewChangeRequestEmail($new_request));
            }
            catch(\Exception $e){
                \Log::info('-------Error Sending Mail to jefatura: '.$e->getMessage());
            }
*/
   return response()->json(["status" => !is_null($new_request)]);
}

public function updateChangeRequest(Request $request){
	$move_request = UserMovementProcess::find($request->request_id);
	if($request->change_type == 0){
                $move_request->new_jefatura_aprove = $request->change >= 2 ? 1 : 0;
        } else {
                $move_request->admin_aprove = $request->change >= 2 ? 1 : 0;
        }
	if($request->change == 1) {
		 $move_request->status = 2;
	} else {
		 if($request->change == 2 && $move_request->new_jefatura_aprove == 1 && $move_request->admin_aprove == 1){
			$move_request->status = 1;
			$cargo =  Cargo::find($move_request->cargo_id);
			$cargo->id_jefatura = $move_request->target_jefatura;
			$cargo->save();
		}
	}
	$user = User::where("id", $move_request->requester_id)->first();
	$admin_empresa = User::where("perfil", '=', 2)->where("empresa_id", '=',$user->empresa_id)->get();
             try{
                \Mail::to($user->email)->cc($admin_empresa)->bcc("j.roure@outlook.cl")
                ->send(new UpdateChangeRequestEmail($move_request));
            }            catch(\Exception $e){
                \Log::info('-------Error Sending Mail to jefatura: '.$e->getMessage());
            }
	return response()->json(["status" =>  $move_request->save()]);
}
    public function import( Request $request){
        if(strtoupper($request->getMethod())=="GET"){
            return view("$this->resource.import")->with(["resource" => $this->resource]);
        }else{
            $file=$request->file("file_file");
            $filename=uniqid().".xlsx";
            $file->move("/tmp/",$filename);
            $import=new $this->importer;
            $antes=$this->clazz::count();
            $import->queue("/tmp/$filename")->chain([
                new NotifyUserOfCompletedImport(auth()->user(),$antes,$this->clazz,$this->resource),
            ]);

            return redirect()->route("$this->resource.index")
                ->with(["message"=>"La carga masiva se esta ejecutando en segundo plano"]);
        }
    }

}
