<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
            $table->unsignedInteger('destinatario_id');
            $table->string('asunto');
            $table->text('descripcion');
            $table->text('adjuntos')->nullable();
            $table->integer('estado')->unsigned()->default(1);
            $table->timestamps();
            $table->foreign("destinatario_id")->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes');
    }
}
