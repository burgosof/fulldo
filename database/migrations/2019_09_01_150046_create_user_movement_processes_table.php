<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMovementProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_movement_processes', function (Blueprint $table) {
            $table->increments('id');
		$table->integer('requester_id');
		$table->integer('cargo_id');
	       // $table->foreign('cargo_id')->references('id')->on('ma_cargo');
		$table->integer('target_jefatura');
                //$table->foreign('target_jefatura')->references('id')->on('ma_cargo');
            
		$table->integer('new_jefatura_aprove');
		$table->datetime('jefatura_aprove_date')->nullable();
		$table->integer('admin_aprove');
		$table->datetime('admin_aprove_date')->nullable();
		$table->integer('status')->default(0);
		$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_movement_processes');
    }
}
